const express = require('express');
const { route } = require('..');
const router = express.Router();
const bicycleApiController = require('../../controllers/api/bicycleApiController');

router.get('/', bicycleApiController.list);
router.post('/create', bicycleApiController.create);
router.post('/update', bicycleApiController.update);
router.delete('/delete', bicycleApiController.delete);



module.exports = router;