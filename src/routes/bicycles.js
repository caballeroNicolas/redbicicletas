const express = require('express');
const router = express.Router();
const bicycleController = require('../controllers/bicycle');

router.get('/', bicycleController.list);
router.get('/crear', bicycleController.create);
router.post('/crear', bicycleController.store);
router.get('/eliminar/:id', bicycleController.delete);
router.get('/editar/:id', bicycleController.update);
router.post('/editar/:id', bicycleController.storeUpdate);




module.exports = router;
