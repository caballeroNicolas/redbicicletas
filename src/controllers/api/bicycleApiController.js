const bicycle = require('../../../models/bicycle');

let apiController = {
    list: (req, res) => {
        res.status(200).json({
            bicycles: bicycle.allbicycles
        });
    },
    create: (req, res) => {
        let bike = new bicycle(req.body.id, req.body.color, req.body.model);
        bike.ubication = [req.body.latitude, req.body.lng];
        bicycle.add(bike);
        res.status(200).json({bike})
    },
    update: (req, res) => {
        let bike = bicycle.findById(req.body.id);
        bike.id = req.body.id;
        bike.color = req.body.color;
        bike.model = req.body.model;
        bike.ubication = [req.body.latitude, req.body.lng];
        res.status(200).json({bike})
    },
    delete: (req, res) => {
        bicycle.removeById(req.body.id);
        res.status(204).send();
    }
};

module.exports = apiController;