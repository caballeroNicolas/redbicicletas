const bicycle = require('../../models/bicycle');

let bicycleController = {
    list: (req, res) => res.render('bicycles/index', {bicycles: bicycle.allbicycles}),
    create: (req, res) => {
        res.render('bicycles/create');
    },
    store: (req, res) => {
        let newBicycle = new bicycle(
            id = req.body.id,
            color = req.body.color,
            model = req.body.model,
        )
        newBicycle.ubication = [req.body.latitude, req.body.lng];
        bicycle.add(newBicycle);
        res.redirect('/bicicletas');
    },
    delete: (req, res) => {
        bicycle.removeById(req.params.id);
        res.redirect('/bicicletas');
    },
    update: (req, res) => {
        let bike = bicycle.findById(req.params.id);
        res.render('bicycles/update', {bike});
    },
    storeUpdate: (req, res) => {
        let bike = bicycle.findById(req.params.id);
        bike.id = req.body.id;
        bike.color = req.body.color;
        bike.model = req.body.model;
        bike.ubication = [req.body.latitude, req.body.lng];
        res.redirect('/bicicletas');
    }
};

module.exports = bicycleController;