const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var bicycleSchema = new Schema({
    code: Number,
    color: String,
    model: String,
    ubication: {
        type:[Number], index: {type: '2dsphere', sparse: true}
    }
});

bicycleSchema.statics.createInstance = function(code, color, model, ubication) {
    return new this({
        code: code,
        color: color,
        model: model,
        ubication: ubication
    });
};

bicycleSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color;
};

bicycleSchema.statics.allBicycles = function(cb) {
    return this.find({}, cb);
};

module.exports = mongoose.model('Bicycles', bicycleSchema);