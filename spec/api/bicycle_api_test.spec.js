const bicycle = require('../../models/bicycle');
const request = require('request');
const server = require('../../bin/www');

describe('bicycleApi', () => {

    describe('GET bicycles', () => {
        it('status 200', () => {
            expect(bicycle.allbicycles.length).toBe(0);

            let bike1 = new bicycle(1, 'black', 'urbana', [-34.6012424, -58.3861497]);
            bicycle.add(bike1);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST bicycles /create', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var bike1 = '{"id":10, "color":"silver", "model":"montaña", "latitude":-34, "lng":-54}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: bike1
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(bicycle.findById(10).color).toBe("silver");
                done()
            });
        });
    });

});