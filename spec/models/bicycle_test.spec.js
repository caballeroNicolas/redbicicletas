const mongoose = require('mongoose');
const Bicycle = require('../../models/bicycle');

describe('Bicycles Testing', () => {
    beforeEach((done) => {
        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true
        });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', () => {
            console.log("We're connected to test db");
            done();
        });
    });

    afterEach((done) => {
        Bicycle.deleteMany({}, (err, succes) => {
            if (err) {
                console.log(err);
            };
            done();
        });
    });


    describe('Bicycles.createInstance', () => {
        it('crea una instancia de Bicycle', () => {
            let bike = Bicycle.createInstance(1, 'green', 'urban', [-34, -54]);

            expect(bike.code).toBe(1);
            expect(bike.color).toBe('green');
            expect(bike.model).toBe('urban');
            expect(bike.ubication[0]).toEqual(-34);
            expect(bike.ubication[1]).toEqual(-54);
        });
    });

    describe('Bicycles.allBicycles', () => {
        it('Starts empty', (done) => {
            Bicycle.allBicycles(function(err, bikes) {
                expect(bikes.length).toBe(0);
                done();
            });
        });
    });
    
});

// beforeEach(() => {
//     bicycle.allbicycles = [];
// });

// describe('bicycle.allbicycles', () => {
//     it('starts empty', () => {
//         expect(bicycle.allbicycles.length).toBe(0);
//     });
// });


// describe('bicycle.add', () => {
//     it('add 1', () => {
//         expect(bicycle.allbicycles.length).toBe(0);

//         let a = new bicycle(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
//         bicycle.add(a);

//         expect(bicycle.allbicycles.length).toBe(1);
//         expect(bicycle.allbicycles[0]).toBe(a);

//     });
// });

// describe('bicycle.findById', () => {
//     it('must return the bike with id 1', () => {
//         expect(bicycle.allbicycles.length).toBe(0);

//         let bike1 = new bicycle(1, 'black', 'urbana', [-34.6012424, -58.3861497]);
//         let bike2 = new bicycle(2, 'silver', 'montaña', [-34.6012424, -58.3861497]);
//         bicycle.add(bike1);
//         bicycle.add(bike2);

//         let bikeToFind = bicycle.findById(1);

//         expect(bikeToFind.id).toBe(1);
//         expect(bikeToFind.color).toBe(bike1.color);
//         expect(bikeToFind.model).toBe(bike1.model);
//     });
// });

// describe('bicycle.removeById', () => {
//     it('must delete the bike with id 1', () => {
//         expect(bicycle.allbicycles.length).toBe(0);

//         let bike1 = new bicycle(1, 'black', 'urbana', [-34.6012424, -58.3861497]);
//         let bike2 = new bicycle(2, 'silver', 'montaña', [-34.6012424, -58.3861497]);
//         bicycle.add(bike1);
//         bicycle.add(bike2);

//         bicycle.removeById(1);

//         expect(bicycle.allbicycles[0].id).toBe(2);
//     });
// });