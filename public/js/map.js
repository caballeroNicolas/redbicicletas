var mymap = L.map('mapid').setView([-34.6157437,-58.5733832], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

L.marker([-34.5683748, -58.4870945]).addTo(mymap);
L.marker([-34.5716619, -58.4842481]).addTo(mymap);


// $.ajax({
//     dataType: "json",
//     url: "api/bicicletas",
//     succes: (result) => {
//         console.log(result);
//         result.bicycles.forEach(bike => {
//             L.marker(bike.ubication).addTo(mymap);
//         });
//     }
// });